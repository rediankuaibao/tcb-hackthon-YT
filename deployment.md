## 项目部署指南

1. 下载本项目或者``` git clone https://gitee.com/tysb7/tcb-hackthon-YT ```
2. 微信小程序后台增加插件```wx069ba97219f66d99```
3. 导入微信小程序开发工具 
4. 修改```/miniprogram/app.js``` 中的 ```env: '云开发环境ID' ```
5. 创建云数据库```busData```,并将```/db/busData.json``` 数据导入


