const plugin = requirePlugin("WechatSI")
const manager = plugin.getRecordRecognitionManager()
import std from '../../utils/std.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tripData: {},
    ttsSwitch: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const db = wx.cloud.database()
    var id = Number(options.id)
    // 根据id查询数据
    db.collection('busData').where({
      _id: id
    }).get().then(res => {
      console.log(res.data[0])
      wx.setStorageSync("tripData", res.data[0])
      this.setData({
        tripData: res.data[0]
      })
    })

    // 从缓存获取ttsSwitch设置
    wx.getStorage({
      key: 'ttsSwitch',
      success: (res) => {
        // 可以查询
        this.setData({
          ttsSwitch: res.data
        })
      },
      fail: (res) => {
         // 无法获取到 说明是第一次访问 则设置为True
        wx.setStorage({
          key: 'ttsSwitch',
          data: true,
        })
      }
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: (res) => {
    const db = wx.cloud.database()
    var id = wx.getStorageSync("tripData")._id
    var path = `pages/trip/index?id=${id}`
    if (res.from === 'button') {
      //上传分享时间
      db.collection('share').add({
          data: {
            time: new Date()
          }
        })
        .then(res => {
          console.log(res)
        })
    } else {
      console.log("来自右上角转发菜单")
    }
    return {
      title: '已经确诊，急寻这几个人！',
      path: path,
      imageUrl: "/images/share.jpg",
      success: (res) => {
        console.log("转发成功", res);
      },
      fail: (res) => {
        console.log("转发失败", res);
      }
    }
  },
  // 复制来源连接
  copyLink(e) {
    wx.setClipboardData({
      data: this.data.tripData.source,
      success(res) {
        wx.vibrateShort()

      }
    })
  },
  shangbao(e) {
    this.tts('请复制来源链接进行上报')
    // wx.navigateTo({
    //   url: `./web-view/index?id=${this.data.tripData.source}`,
    // })
    wx.showModal({
      title: '提示',
      content: '复制来源链接进行上报',
      success: (res) => {
        if (res.confirm) {
          this.copyLink();
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // tts
  tts(e) {
    if (this.data.ttsSwitch) {
      std.Voice(e)
    }
  },
  back() {
    wx.navigateBack({
    })
  },
  backHome () {
    wx.reLaunch({
      url: '/pages/wuhan/index',
    })
  },
})